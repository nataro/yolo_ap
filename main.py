import os
import sys
import time
from logging import getLogger, StreamHandler, FileHandler, DEBUG, INFO
from typing import Optional, Tuple, Sequence, List

from datetime import datetime
from pathlib import Path
import numpy as np

import cv2

import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn

from PyQt5.QtCore import QTimer, pyqtSignal, QObject, QBuffer, QUrl
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QDialog, QApplication, QFileDialog, QMessageBox
from PyQt5.uic import loadUi

# from PyQt5 import QtMultimedia  # 音を出すために追加
import winsound  # 音を出すために追加

from models.experimental import attempt_load

from utils.torch_utils import select_device, time_sync
from utils.general import check_img_size, check_suffix, non_max_suppression, scale_coords

from utils.augmentations import letterbox
from utils.plots import Annotator, colors


from dev.cam_capture import CaptureDevice

# 相対パスを絶対パスに変換: resolve()
# 絶対パスを相対パスに変換: relative_to()
FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]  # root directory
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
ROOT = ROOT.relative_to(Path.cwd())  # relative


class YoloApp(QDialog):
    """
    それぞれ異なるシェイプの画像を処理することになるので意識して画像やテンソルを使い分けないといけない
    キャプチャ画像のシェイプ：　例　(1080, 810, 3)
    GUI表示用のシェイプ：　例　(680, 480, 3)
    予測に用いる画像のシェイプ：　例　(3, 640, 480)　←　順番に注意


    """

    def __init__(
            self,
            cam_num: int = 0,
            cap_size: Tuple[int, int] = (640, 480),  # カメラから取得する画像のサイズ
            disp_size: Optional[Tuple[int, int]] = None,  # GUIに表示する画像のサイズ
            infer_upper_size: int = 640,  # 予測に用いる画像のサイズ。上限。(pixels)
            cap_mode: str = 'dshow',
            frame_interval: int = 50,  # Set frame interval To 200 ms == 0.2 second
            weights=ROOT / 'best.pt',
            device='cpu',  # cuda device, i.e. 0 or 0,1,2,3 or cpu

    ):

        # UI初期化処理
        super().__init__()
        loadUi('yolo_app.ui', self)
        self.init_ui()

        # 定期コールバック用
        self._timer = QTimer(self)

        # ビープ音関連
        self._beep_frequency = 3500  # Set Frequency To 2500 Hertz
        self._beep_duration = 100  # Set Duration To 100 ms == 0.1 second

        # 画像関連
        self._cam_num = cam_num
        self._cap_size = cap_size
        self._cap_mode = cap_mode
        self._disp_size = cap_size if disp_size is None else disp_size
        self._infer_upper_size = infer_upper_size
        # self._imgsz = imgsz
        # self._cap_time_msec = cap_time_msec
        self._frame_interval = frame_interval

        self._empty_img = np.ones((self._cap_size[1], self._cap_size[0], 3), np.uint8) * 255

        self._cap_dev = CaptureDevice(
            cam_num=self._cam_num,
            width=self._cap_size[0],
            height=self._cap_size[1],
            cap_mode=self._cap_mode,
        )

        # 検出パラメータだと思う
        self._conf_thres = 0.25  # confidence threshold
        self._iou_thres = 0.45  # NMS IOU threshold
        self._classes = None  # filter by class: --class 0, or --class 0 2 3
        self._agnostic_nms = False  # class-agnostic NMS
        self._max_det = 1000  # maximum detections per image
        self._line_thickness = 3  # bounding box thickness (pixels)
        self._hide_labels = False  # hide labels
        self._hide_conf = False  # hide confidences

        self._weights = weights
        self._device = device
        self._model = ...
        self._stride = 64  # assign defaults
        self._names = [f'class{i}' for i in range(1000)]  # assign defaults

        self.model_init()

    @staticmethod
    def get_inference_img(src_img, img_upper_size=640, stride: int = 32, auto: bool = True):
        '''
        datasets.LoadImagesを参考にした

        元画像をリサイズして、推論に用いるための画像を取得する。以下のような次元の入れ替えも行う。

        元画像シェイプ：　例　(680, 480, 3)
        予測に用いる画像のシェイプ：　例　(3, 640, 480)　←　順番に注意

        '''
        # Padded resize
        img = letterbox(src_img, img_upper_size, stride=stride, auto=auto)[0]

        # Convert
        img = img.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
        # np.ascontiguousarray : メモリの格納の順番を入れ替えている？使用バイト数や計算速度に影響？
        img = np.ascontiguousarray(img)

        return img

    @torch.no_grad()
    def model_init(self):
        self._device = select_device(self._device)
        # Load model
        w = self._weights[0] if isinstance(self._weights, list) else self._weights
        suffix, suffixes = Path(w).suffix.lower(), ['.pt', ]
        check_suffix(w, suffixes)  # check weights have acceptable suffix

        # '.pt'前提で進める
        self._model = attempt_load(self._weights, map_location=self._device)  # load FP32 model
        self._stride = int(self._model.stride.max())  # model stride
        self._names = self._model.module.names if hasattr(self._model,
                                                          'module') else self._model.names  # get class names

        self._infer_upper_size = check_img_size(self._infer_upper_size, s=self._stride)  # check image size
        print(self._stride)
        print(self._names)

    @torch.no_grad()
    def run(self, img):
        '''
        webカメラから取り込んだ画像データのshapeは(480, 640, 3)になっている。
        この後の処理のために(3, 480, 640)と入れ替えておかないといけないみたい
        '''

        # img = img.transpose(2, 0, 1)
        # # print(img.shape)

        if self._device.type != 'cpu':
            self._model(
                torch.zeros(1, 3, *self._infer_upper_size).to(self._device).type_as(
                    next(self._model.parameters())))  # run once

        dt, seen = [0.0, 0.0, 0.0], 0

        t1 = time_sync()
        img = torch.from_numpy(img).to(self._device)
        img = img.float()  # uint8 to fp16/32

        img = img / 255.0  # 0 - 255 to 0.0 - 1.0
        if len(img.shape) == 3:
            img = img[None]  # expand for batch dim
        t2 = time_sync()
        dt[0] += t2 - t1

        # ここで与えるimgのshapeは[1, 3, 480, 640]のような形（最初の次元はバッチサイズ）
        # print(img.shape)
        # pred.shape -> torch.Size([1, 18900, 7])
        pred = self._model(img, augment=False, visualize=False)[0]
        t3 = time_sync()
        dt[1] += t3 - t2

        # print(pred.shape)
        # print(type(pred))

        '''
        non_max_suppression()の返り値が検出結果と思われる
        以下のような 'torch.Tensor' がList型の中に格納されている（1要素のみ？）
        List型に格納される要素の個数はバッチサイズ（画像の個数）によるのだと思う。
        今回は1画像づつ処理するので1要素しか入っていない。
        
        tensor([
            ...,
            [239.01186, 243.58472, 339.41193, 338.18256,   0.64131,   0.00000]
            ...,
            ])
            
        [枠に関する情報（4つ）, 確率, クラス]が検出したオブジェクトの個数分入っているのだと思う
        '''
        # NMS
        pred = non_max_suppression(
            pred,
            self._conf_thres,
            self._iou_thres,
            self._classes,
            self._agnostic_nms,
            max_det=self._max_det,
        )
        dt[2] += time_sync() - t3

        # print(dt)
        # print(type(pred))
        # print('len', len(pred))
        # print('pre', pred)

        return pred

    def init_ui(self):
        self.btnStart.clicked.connect(self.start_webcam)
        self.btnStop.clicked.connect(self.stop_webcam)

    def start_webcam(self):
        self._cap_dev.open()
        self._timer.timeout.connect(self.update_frame)
        self._timer.start(self._frame_interval)

    def stop_webcam(self):
        self.display_image(self._empty_img, 1)
        # self.display_image(self._empty_img, 2)
        self._cap_dev.close()
        self._timer.stop()

    def update_frame(self):
        cap_img = self._cap_dev()
        if cap_img is None:
            return
        cap_img = cv2.flip(cap_img, 1)

        # print(self._disp_size)
        disp_img = cv2.resize(cap_img, dsize=self._disp_size)

        # 予測用にリサイズし、次元の順番を入れ替えた画像を取得
        infer_img = self.get_inference_img(disp_img, self._infer_upper_size, self._stride)
        disp_img, cls_list = self.image_inferance(infer_img, disp_img)

        # 検出したクラスの中に「1」が含まれていたらビープ音を鳴らす
        if 1 in cls_list:
            winsound.Beep(self._beep_frequency, self._beep_duration)

        # GUIに表示
        self.display_image(disp_img, window=1)

    def image_inferance(self, img, org_img) -> Tuple[np.ndarray, List]:
        # 予測実行
        pred = self.run(img)

        detected_classes = []  # 検出したクラス格納しておく
        for i, det in enumerate(pred):  # per image
            # AnnotatorクラスはAnotatoer.result()でアノテーションした画像を返すみたい
            annotator = Annotator(org_img, line_width=self._line_thickness, example=str(self._names))
            if len(det):
                # img.shape -> (ch, h, w)
                # org_img.shape -> (h, w, ch)
                # Rescale boxes from img_size to org_img size
                det[:, :4] = scale_coords(img.shape[1:], det[:, :4], org_img.shape[:2]).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s = f"{n} {self._names[int(c)]}{'s' * (n > 1)}, "  # add to string

                # Write results
                for *xyxy, conf, cls in reversed(det):
                    c = int(cls)  # integer class
                    label = None if self._hide_labels else (
                        self._names[c] if self._hide_conf else f'{self._names[c]} {conf:.2f}')
                    annotator.box_label(xyxy, label, color=colors(c, True))
                    detected_classes.append(cls)

            # self._mediaPlayer.stop()

            return annotator.result(), detected_classes

    def display_image(self, img, window=1):
        qformat = QImage.Format_Indexed8
        if len(img.shape) == 3:  # rows[0], cols[1], channels[2]
            if img.shape[2] == 4:
                qformat = QImage.Format_RGBA8888
            else:
                qformat = QImage.Format_RGB888
        out_image = QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
        # BGR -> RGB
        out_image = out_image.rgbSwapped()

        if window == 1:
            self.lblImage.setPixmap(QPixmap.fromImage(out_image))
            self.lblImage.setScaledContents(True)
        if window == 2:
            self.lblProcessed.setPixmap(QPixmap.fromImage(out_image))
            self.lblProcessed.setScaledContents(True)


if __name__ == "__main__":
    # アプリケーション作成
    app = QApplication(sys.argv)

    # オブジェクト作成
    window = YoloApp()
    window.setWindowTitle('light detect')
    # MainWindowの表示
    window.show()
    # MainWindowの実行
    sys.exit(app.exec_())
