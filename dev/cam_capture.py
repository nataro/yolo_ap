import time
from typing import Optional
from logging import getLogger, StreamHandler, FileHandler, DEBUG

import cv2
import numpy as np

from dev.callable_device import CallableDevice

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
logger.addHandler(handler)

class CaptureDevice(CallableDevice):

    def __init__(self,
                 cam_num: int = 0,
                 width: int = 1920,
                 height: int = 1080,
                 fps: int = 12,
                 buffer_size: Optional[int] = None,
                 cap_mode: str = 'dshow',
                 codec: str = 'mjpg', ):
        self._cam_num = cam_num
        self._width = width
        self._height = height
        self._fps = fps
        self._codec = codec
        self._buffer_size = buffer_size  # 0は制限なし、1が最小で数が増えると増加すると思う
        self._cap_mode = cap_mode
        self._video_cap = None

    def __call__(self) -> Optional[np.ndarray]:
        if self.open():
            # # フレームの遅延がある場合は、read()を何度か実行すると解決するかもしれない
            # for _ in range(50):
            #     ret, frame = self._video_cap.read()
            ret, frame = self._video_cap.read()
            if ret:
                return frame
        raise IOError('Can not capture the image.')
        return None

    def open(self) -> bool:
        return self.open_cam()

    def close(self) -> None:
        return self.close_cam()

    def cam_capture(self) -> cv2.VideoCapture:
        '''
        cv2.CAP_DSHOWを付けないと[ WARN:0] global ... async callback
        というワーニングが出る（撮影回数が増えると動作が遅くなる原因か？）
        ただし、cv2.CAP_DSHOWにすると
            ・画像の明るさが低下する
            ・cam.open()やキャプチャの速度が非常に遅い
        という問題がある。
        画像の明るさは、dino-lightの場合
            BRIGHTNESS : 128->130.0
            CONTRAST : 16.0->17.0
        と変更することで若干改善する（EXPOSUREは変更できないみたい）
        '''
        cap_mode_dict = {
            'dshow': cv2.CAP_DSHOW,
            'msmf': cv2.CAP_MSMF,
            'v4l2': cv2.CAP_V4L2,
        }
        cap_mode = cap_mode_dict.get(self._cap_mode, cv2.CAP_DSHOW)
        cap = cv2.VideoCapture(self._cam_num, cap_mode)
        cap.set(cv2.CAP_PROP_FPS, self._fps)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, self._width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, self._height)

        codec_dict = {
            'mjpg': cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
            'h264': cv2.VideoWriter_fourcc('H', '2', '6', '4'),
            'yuyv': cv2.VideoWriter_fourcc('Y', 'U', 'Y', 'V'),
        }
        codec = codec_dict.get(self._codec, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        cap.set(cv2.CAP_PROP_FOURCC, codec)

        if self._buffer_size is not None:
            '''
            バッファサイズの指定は、画像取得に遅延が起きてきた場合の対策になる
            特にread()の実行回数が少ないような場合は、バッファサイズを小さくして遅延を少なくする
            バッファサイズを小さくしても遅延が激しいようであれば、多分read()を回し続けて、
            最新の画像を取り込めるような工夫をしないと行けないと思う 
            多分「1」が最小の指定になると思う
            '''
            # バッファサイズを小さくして遅延を少なくする
            cap.set(cv2.CAP_PROP_BUFFERSIZE, self._buffer_size)

        return cap

    def is_open(self) -> bool:
        return False if self._video_cap is None else self._video_cap.isOpened()

    def open_cam(self) -> bool:
        if not self.is_open():
            self._video_cap = self.cam_capture()
        return self.is_open()

    def close_cam(self) -> None:
        if self._video_cap:
            self._video_cap.release()
            cv2.destroyAllWindows()  # Handles the releasing of the camera accordingly

    def __enter__(self):
        """
        with 文開始時にコールされる。
        ここで返す値なりオブジェクトを with 文の as エイリアスで受けることができる
        """
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        exc_type, exc_value, traceback は with 文内で例外が発生した際に例外情報を受取る
        例外が発生しないときはすべて None がセットされる
        """
        self.close()



class MicroscopeCaptureDevice(CaptureDevice):
    """
    Microscopeカメラデバイスはcall()で呼び出して撮影を行うときに、デバイスを自動でopenし、
    終了後に自動でcloseするような運用をするので、self.open()やself.close()による
    操作は無効になるように実装している
    """

    def __init__(self,
                 cam_num: int = 0,
                 width: int = 1280,
                 height: int = 1024,
                 fps: int = 15,
                 buffer_size: Optional[int] = None,
                 codec: str = 'mjpg', ):
        super().__init__(cam_num, width, height, fps, buffer_size, codec)

    def __call__(self) -> Optional[np.ndarray]:

        if self.open_cam():
            # 50フレームくらい回して安定させる
            # for _ in range(50):
            for _ in range(30):
                ret, frame = self._video_cap.read()
            # open、closeを何度か実行しないとLEDライトが確実に切れない
            self.close_cam()
            self.open_cam()
            self.close_cam()
            time.sleep(1)  # カメラが安定するまでしばらく待機
            if ret:
                return frame
        return None

    def open(self) -> bool:
        """
        カメラは実行時に自動的にopenし、撮影が終わったらcloseするように運用する
        外部から操作できないようにこの関数は常にFalseを返すようにしておく
        """
        return False

    def close(self) -> None:
        """
        カメラは実行時に自動的にopenし、撮影が終わったらcloseするように運用する
        外部から操作できないようにこの関数は常にNoneを返すようにしておく
        """
        return None


# class CaptureDeviceDirector(object):
#     def __init__(self,
#                  capture_device: CallableDevice,
#                  ):
#         self._capture_device = capture_device
#
#     def cap(self):
#         if not isinstance(self._capture_device, CallableDevice):
#             raise TypeError('A appropriate capture device is not set up.')
#
#         img = self._capture_device()
#         if img is None:
#             raise IOError('Can not capture the image.')
#
#         return img
#
#     def __enter__(self):
#         """
#         with 文開始時にコールされる。
#         ここで返す値なりオブジェクトを with 文の as エイリアスで受けることができる
#         """
#         self._capture_device.open()
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         """
#         exc_type, exc_value, traceback は with 文内で例外が発生した際に例外情報を受取る
#         例外が発生しないときはすべて None がセットされる
#         """
#         self._capture_device.close()


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    import time

    # cap_dev = MicroscopeCaptureDevice()
    cap_dev = CaptureDevice()

    print('cap')

    img = cap_dev()
    print(img)
    print(type(img))

    time.sleep(5)

    print('finished')
